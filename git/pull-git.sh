#!/bin/bash

cd "$(dirname "$0")"

# Usage ./pull-git.sh SOURCE TARGET
# Where SOURCE = Base uri of source repositories
# Where TARGET = Directory of target repositories
# e.g. ./pull-git.sh "/s/Software\ Development/Git" /d/Work/gittesting/source

SOURCE=$1
TARGET=$2

while read p; do
	if [ -d "$TARGET/$p" ]; then
		cd "$TARGET/$p"
		echo "Fetching changes from $SOURCE/$p to $TARGET/$p"
		git fetch --all
		#echo "Pulling changes from $SOURCE/$p to $TARGET/$p"
		#git pull "$SOURCE/$p" master
	fi
done < ./repos
