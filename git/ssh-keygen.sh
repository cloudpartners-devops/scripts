#!/bin/bash

if [ ! -f ~/.ssh/id_rsa ]; then
	echo "Generating ssh keypair"
	ssh-keygen -q -t rsa -f ~/.ssh/id_rsa -N ""
fi

echo
echo "Wonderful!  Now, please paste the contents of your clipboard into an email and send it to Steve Chapman (Steve.Chapman@saiglobal.com)."
cat ~/.ssh/id_rsa.pub > /dev/clipboard
