#!/bin/bash

cd "$(dirname "$0")"

# Usage ./sync-git.sh SOURCE SANDBOX TARGET
# Where SOURCE = Directory of repositories
# Where SANDBOX = Directory of sandbox repositories
# Where TARGET = Repository target base url
# e.g. ./sync-git.sh "/s/Software\ Development/Git" /d/Work/gittesting/source ssh://git@localhost:2200/srv/git

SOURCE=$1
SANDBOX=$2
TARGET=$3

#echo "Cleaning the sandbox repository hive"
#./clean-git.sh $SANDBOX
echo "Fetching changes from source repositories to sandbox repositories"
./fetch-git.sh "$SOURCE" "$SANDBOX"
echo "Pushing changes from sandbox repositories to target repositories"
./push-git.sh "$SANDBOX" "$TARGET"
