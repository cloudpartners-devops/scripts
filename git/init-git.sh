#!/bin/bash

cd "$(dirname "$0")"

# Usage ./init-git.sh SOURCE TARGET
# Where SOURCE = Base path of repository sources
# Where TARGET = Repository target base url
# e.g. ./init-git.sh /d/Work/gittesting/source

SOURCE=$1

echo "Running init-git $SOURCE"

while read p; do
	cd "$SOURCE"
	if [ -d $p ]; then
		rm -rf $p
	fi
	echo "Initialising $SOURCE/$p"
	mkdir -p "$SOURCE/$p"
	cd "$SOURCE/$p" 
	git init --bare
done < ./repos