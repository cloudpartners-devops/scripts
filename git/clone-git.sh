#!/bin/bash

cd "$(dirname "$0")"

# Usage ./clone-git.sh SOURCE TARGET
# Where SOURCE = Base path of repository sources
# Where TARGET = Repository target base url
# e.g. ./clone-git.sh "/s/Software\ Development/Git" /d/Work/gittesting/source

SOURCE=$1
TARGET=$2

echo "Running clone-git $SOURCE $TARGET"

while read p; do
	cd "$TARGET"
	if [ -d $p ]; then
		rm -rf $p
	fi
	echo "Cloning $SOURCE/$p"
	git clone --bare "$SOURCE/$p" $p
done < ./repos