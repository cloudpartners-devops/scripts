#!/bin/bash

REPOLIST=$1
TARGET=$2

id -u git &>/dev/null || useradd -m git

sudo chpasswd << 'END'
git:git
END

# git file location
if [ ! -d $TARGET ]; then
	mkdir -p $TARGET
fi

while read p; do
	if [ ! -d $TARGET/$p.git ]; then
		mkdir -p $TARGET/$p.git
		cd $TARGET/$p.git
		git --bare init
	fi
done < $REPOLIST

chown -R git:git $TARGET
