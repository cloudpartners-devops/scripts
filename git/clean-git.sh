#!/bin/bash

cd "$(dirname "$0")"

# Usage ./clean-git.sh SOURCE
# Where SOURCE = Base path of repository sources
# e.g. ./clean-git.sh /d/Work/gittesting/source

SOURCE=$1

while read p; do
	if [ -d "$SOURCE/$p" ]; then
		echo "Cleaning $SOURCE/$p"
		cd "$SOURCE/$p"
		git reset --hard
		git clean -fxd
	fi
done < ./repos
