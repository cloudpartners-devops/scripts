#!/bin/bash

cd "$(dirname "$0")"

SOURCEDIR=$1

while read p; do
	if [ ! -f $SOURCEDIR/$p/.gitattributes ]; then
		echo "Copying .gitattributes to $SOURCEDIR/$p/.gitattributes"
		cp ./.gitattributes $SOURCEDIR/$p/.gitattributes

		cd $SOURCEDIR/$p
		git add .
		git commit -m 'Add .gitattributes'
		git rm --cached -r .
		git reset --hard
		git add .
		git commit -m 'Normalise line endings'
		
	fi
done < ./realrepos