#!/bin/bash

cd "$(dirname "$0")"

# Usage ./push-git.sh SOURCE TARGET
# Where SOURCE = Directory of repositories
# Where TARGET = Repository target base url
# e.g. ./push-git.sh /d/Work/gittesting/source ssh://git@localhost:2200/srv/git

SOURCE=$1
TARGET=$2

while read p; do
	if [ -d "$SOURCE/$p" ]; then
		cd "$SOURCE/$p"
		echo "Pushing changes from $SOURCE/$p to $TARGET/$p"
		git push "$TARGET/$p" --all
	fi
done < ./repos
