#!/bin/bash

cd /tmp
sudo rm -f /tmp/Console.zip*

echo "Fetching latest console build"
if [ -d /usr/local/bin/xoura-mono-console ]; then
	sudo rm -rf /usr/local/bin/xoura-mono-console
fi

wget https://buildservice:Bu1ldS3rv1c3@teamcity.cloudpartners.net.au/repository/download/CloudPartners_Xoura_XouraMono_BuildAndPackage/.lastSuccessful/Console.zip

sudo unzip Console.zip -d /usr/local/bin/xoura-mono-console

# echo "Fetching latest provisioning scripts"
#if [ -d /usr/local/bin/scripts ]; then
#	sudo rm -rf /usr/local/bin/scripts
#fi

#sudo git clone https://source.cloudpartners.net.au/scm/util/scripts.git /usr/local/bin/scripts

USERID="monouser"

id -u $USERID &>/dev/null || sudo useradd -r -s /bin/false -g sudo $USERID
echo 'monouser ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/monouser
chmod 0440 /etc/sudoers.d/monouser

if [ -z $1 ]; then
 ENVIRONMENT="intg"
else
 ENVIRONMENT="$1"
fi

if [ -z $2 ]; then
 PORT="8081"
else
 PORT="$2"
fi

DESCRIPTION="xoura-mono-console-$ENVIRONMENT-$PORT"
AUTHOR="Steve Chapman - Cloud Partners Pty Ltd 2014"
DAEMON="/usr/bin/mono"
DAEMONARGS="/usr/local/bin/xoura-mono-console/Xoura.Mono.Console.exe http://*:$PORT/"

# config file script
wget -O /tmp/"$ENVIRONMENT".conf https://gist.githubusercontent.com/chappoo/9987505/raw/93961374e5496b67016c350c3236455dd446feb4/upstart-template.conf

echo "Description : $DESCRIPTION"
echo "Author      : $AUTHOR"
echo "UserID      : $USERID"
echo "Daemon      : $DAEMON"
echo "Args        : $DAEMONARGS"
echo "Environment : $ENVIRONMENT"

# substitute config tokens
sed -e "s|{{Description}}|$DESCRIPTION|g" \
    -e "s|{{Author}}|$AUTHOR|g" \
    -e "s|{{UserId}}|$USERID|g" \
    -e "s|{{Daemon}}|$DAEMON|g" \
    -e "s|{{DaemonArgs}}|$DAEMONARGS|g" \
    < /tmp/"$ENVIRONMENT".conf > /etc/init/xoura-mono-console-"$ENVIRONMENT".conf

sudo start xoura-mono-console-"$ENVIRONMENT"
