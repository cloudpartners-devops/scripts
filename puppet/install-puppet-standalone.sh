#!/bin/bash

if [ ! -f /usr/bin/puppet ]; then
	echo "If you're running this script with vagrant it's recommended you use a base box with puppet pre-installed instead"
	echo "Installing Puppet stand-alone"
	wget -q --no-proxy https://apt.puppetlabs.com/puppetlabs-release-precise.deb
	dpkg -i puppetlabs-release-precise.deb
	apt-get update -y -q > /dev/null
	apt-get install -y -q puppet-common > /dev/null
	echo "Puppet stand-alone has been installed"
else
	echo "Puppet is already installed - skipping"
fi
