#!/bin/bash

response=$(curl --write-out %{http_code} --silent --output /dev/null $1)

if [ "$response" == "200" ]; then
	echo "OK     : $response : $1"
	exit 0
else
	echo "Broken : $response : $1"
	exit 1
fi