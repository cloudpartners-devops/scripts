#!/bin/bash

if [ "$(whoami)" != "root" ]; then
 echo "ERROR : Run script as Root (sudo !!) please"
 exit 1
fi

if [ -z $1 ]; then
 IP="127.0.0.1"
else
 IP="$1"
fi

echo "Using IP address $IP"

if [ -z $2 ]; then
 PORT="6379"
else
 PORT="$2"
fi

echo "Using port number $PORT"

if [ -z $3 ]; then
 PASSWORD=""
else
 PASSWORD="$3"
fi

ADDRESS="$IP"."$PORT"
ADDRESSHYPHEN=$(echo $ADDRESS | sed -r 's/[.]+/-/g')

if [ -f /etc/init.d/redis-server-"$ADDRESSHYPHEN" ]; then
	echo "REDIS configuration already exists for the specified IP address and PORT combination"
	exit 1
fi

id -u redis &>/dev/null || useradd -r -s /bin/false redis

# pid file location
if [ ! -d /var/run/redis ]; then
	mkdir /var/run/redis
fi
chown -R redis:redis /var/run/redis/

# log file location
if [ ! -d /var/log/redis ]; then
	mkdir /var/log/redis
fi
chown -R redis:redis /var/log/redis/

# dump file location
if [ ! -d /var/redis ]; then
	mkdir /var/redis
fi
chown -R redis:redis /var/redis/

# config file location
if [ ! -d /etc/redis ]; then
	mkdir /etc/redis
fi

# config file script
wget -O /tmp/"$ADDRESS".conf https://gist.githubusercontent.com/chappoo/9870414/raw/d7470ee024a051834470922f9c898d17f798666b/redis_conf_template.sh

# substitute config tokens
sed -e "s/{{IP}}/$IP/g" \
    -e "s/{{PORT}}/$PORT/g" \
    -e "s/{{ADDRESS}}/$ADDRESS/g" \
    -e "s/{{ADDRESSHYPHEN}}/$ADDRESSHYPHEN/g" \
    -e "s/{{PASSWORD}}/$PASSWORD/g" \
    < /tmp/"$ADDRESS".conf > /etc/redis/"$ADDRESS".conf

chown -R redis:redis /etc/redis/

# start up script
wget -O /tmp/redis-server-"$ADDRESSHYPHEN" https://gist.githubusercontent.com/chappoo/9871880/raw/af2f2dd814b4344c85410530104f95d7d3349130/redis_startup_template_2.sh

# substitute startup tokens
sed -e "s/{{IP}}/$IP/g" \
    -e "s/{{PORT}}/$PORT/g" \
    -e "s/{{ADDRESS}}/$ADDRESS/g" \
    -e "s/{{ADDRESSHYPHEN}}/$ADDRESSHYPHEN/g" \
    < /tmp/redis-server-"$ADDRESSHYPHEN" > /etc/init.d/redis-server-"$ADDRESSHYPHEN"

chmod 755 /etc/init.d/redis-server-"$ADDRESSHYPHEN"
update-rc.d redis-server-"$ADDRESSHYPHEN" defaults
service redis-server-"$ADDRESSHYPHEN" start
