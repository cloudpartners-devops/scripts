#!/bin/bash

if [ -f /usr/local/bin/redis-server ]; then
	echo "Detected an existing redis-server instance, skipping installation"
	exit 0
fi

if [ "$(whoami)" != "root" ]; then
 echo "ERROR : Run script as Root (sudo !!) please"
 exit 1
fi

if [ -z $1 ]; then
 VERSION="2.8.8"
else
 VERSION=$1
fi

echo "Using REDIS version v.$VERSION"

# installing build essentials if it is missing
apt-get -y -q install build-essential

wget http://download.redis.io/releases/redis-$VERSION.tar.gz
tar xzf redis-$VERSION.tar.gz
cd redis-$VERSION
make
make install prefix=/usr/local/bin/

cd ..
rm redis-$VERSION -R
rm redis-$VERSION.tar.gz
