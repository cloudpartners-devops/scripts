#!/bin/bash

if [ "$(whoami)" != "root" ]; then
 echo "ERROR : Run script as Root (sudo !!) please"
 exit 1
fi

if [ -z $1 ]; then
 IP="127.0.0.1"
else
 IP="$1"
fi

echo "Using IP address $IP"

if [ -z $2 ]; then
 PORT="6379"
else
 PORT="$2"
fi

echo "Using port number $PORT"

ADDRESS="$IP"."$PORT"
ADDRESSHYPHEN=$(echo $ADDRESS | sed -r 's/[.]+/-/g')

if [ ! -f /etc/init.d/redis-server-"$ADDRESSHYPHEN" ]; then
	echo "Could not find REDIS configuration for the specified IP address and PORT combination"
	exit 1
fi

echo "Stopping the service for redis-server-$ADDRESSHYPHEN"
service redis-server-"$ADDRESSHYPHEN" stop

echo "Removing the service for redis-server-$ADDRESSHYPHEN"
rm -f /etc/init.d/redis-server-"$ADDRESSHYPHEN"
update-rc.d redis-server-"$ADDRESSHYPHEN" remove

if [ -f /etc/redis/"$ADDRESS".conf ]; then
	echo "Removing the configuration for redis-server-$ADDRESSHYPHEN"
	rm -f /etc/redis/"$ADDRESS".conf
fi

if [ -f /var/redis/"$ADDRESS".rdb ]; then
	echo "Removing the dump file for redis-server-$ADDRESSHYPHEN"
	rm -f /var/redis/"$ADDRESS".rdb
fi

if [ -f /var/log/redis/"$ADDRESS".log ]; then
	echo "Removing the log file for redis-server-$ADDRESSHYPHEN"
	rm -f /var/log/redis/"$ADDRESS".log
fi

if [ -f /var/run/redis/"$ADDRESS".pid ]; then
	echo "Removing the pid file for redis-server-$ADDRESSHYPHEN"
	rm -f /var/run/redis/"$ADDRESS".pid
fi
