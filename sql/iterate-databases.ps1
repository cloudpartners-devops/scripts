﻿if ( Get-PSSnapin -Registered | where {$_.name -eq 'SqlServerProviderSnapin100'} ) 
{ 
    if( !(Get-PSSnapin | where {$_.name -eq 'SqlServerProviderSnapin100'})) 
    {  
        Add-PSSnapin SqlServerProviderSnapin100 | Out-Null 
    } ;  
    if( !(Get-PSSnapin | where {$_.name -eq 'SqlServerCmdletSnapin100'})) 
    {  
        Add-PSSnapin SqlServerCmdletSnapin100 | Out-Null 
    } 
} 
else 
{ 
    if( !(Get-Module | where {$_.name -eq 'sqlps'})) 
    {  
        Import-Module 'sqlps' –DisableNameChecking 
    } 
}

$Servers = "LOCALHOST\LOCAL"
$Query = "SELECT SERVERPROPERTY('ServerName') AS ServerName
                ,SERVERPROPERTY('ProductVersion') AS ProductVersion
                ,SERVERPROPERTY('ProductLevel') AS ProductLevel
                ,SERVERPROPERTY('Edition') AS Edition
                ,SERVERPROPERTY('EngineEdition') AS EngineEdition;"

$Servers | ForEach-Object {
    $server = "$_";
    Set-Location SQLSERVER:\SQL\$server
    Invoke-Sqlcmd -Query $Query -ServerInstance $server;
}

$Servers | ForEach-Object {
    $server = "$_";
    Set-Location SQLSERVER:\SQL\$server\DATABASES
    Get-ChildItem
}
